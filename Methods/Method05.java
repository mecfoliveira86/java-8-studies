//Implement a method sumInRange for calculating the sum of numbers in the range from (inclusive), to (exclusive).

import java.util.Scanner;

public class Method05 {

    /**
     * The method calculates the sum of integers in a given range
     * 
     * @param from inclusive
     * @param to exclusive
     *
     * @return the sum (long)
     */
    public static long sumInRange(int from, int to) {
        long result = 0;
        if (from == to) {
            return 0;
        } else {
            for (int i = from; i < to; i++) {
                result += i;
            }
            return result;
        }
    }

    /* Do not change code below */
    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);

        int from = scanner.nextInt();
        int to = scanner.nextInt();

        System.out.println(sumInRange(from, to));
    } 
}
