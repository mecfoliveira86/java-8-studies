//Implement a method that checks whether a given English letter is a vowel or not. The input may be in any case.

//In our case, the letter 'y' is not considered a vowel.

//Examples:

//isVowel('a') should be true
//isVowel('A') should be true
//isVowel('b') should be false

import java.util.Scanner;

public class Method04 {

    public static boolean isVowel(char ch) {
        String data = String.valueOf(ch);
        data.toUpperCase();
    	String reference = "AEIOUaeiou";
        return reference.contains(data);
    }

    /* Do not change code below */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        char letter = scanner.nextLine().charAt(0);
        System.out.println(isVowel(letter) ? "YES" : "NO");
    }
}
