//You're given the method power that takes two int numbers n and m. The method should return the value of nm as a long value.

//Write a body of the method.

import java.util.Scanner;

public class Method09 {

    public static long power(int n, int m) {
        long result = 0;
        if (m == 0) {
            return 1;
        } else {
            result = (long) Math.pow(n, m);
            return result;
        }
    }

    /* Do not change code below */
    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);
        final int n = scanner.nextInt();
        final int m = scanner.nextInt();
        System.out.println(power(n, m));
    }
}
