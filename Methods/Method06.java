//Here is the method named extractInt that takes a double value and returns an integer value.
//Write just a body of the method. It should return only the integer part of the given value.

import java.util.Scanner;

public class Method06 {

    public static int extractInt(double d) {
        double fractional = d % 1;
        double result = d - fractional;
        return (int) result;
    }

    /* Do not change code below */
    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);
        final double d = scanner.nextDouble();
        System.out.println(extractInt(d));
    }
}
