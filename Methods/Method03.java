//Write a method that tests the given number is composite or not. The composite number is a positive integer that has at least one divisor other than 1 and itself.
//The method should return a value of the boolean type.

import java.util.Scanner;

public class Method03 {

    public static boolean isComposite(long number) {
        long counter = 0;
        for (long i = 1; i <= number; i++) {
            if (number % i == 0) {
                counter += 1;
            }
        }
        if (counter > 2) {
            return true;
        } else {
            return false;
        }
    }

    /* Do not change code below */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        final long number = scanner.nextLong();
        System.out.println(isComposite(number));
    }
}