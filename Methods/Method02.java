//Write a method that calculates the factorial of a given number.
//The factorial of n is calculated by the product of integer number from 1 to n (inclusive). The factorial of 0 is equal to 1.

import java.util.Scanner;

public class Method02 {

    public static long factorial(long n) {
        long value = 1;
        if (n == 0) {
            return 1;
        } else {
            for (long i = 1; i <= n; i++) {
                value *= i;
            }
            return value;
        }
    }

    /* Do not change code below */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        long n = Integer.parseInt(scanner.nextLine().trim());
        System.out.println(factorial(n));
    }
}