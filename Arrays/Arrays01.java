//Initialize an int array named numbers with five elements 12, 17, 8, 101, 33 and output it.

//Use the provided code template.

import java.util.Arrays;

public class Arrays01 {

    public static void main(String[] args) {

        int[] numbers; // initialize the array
        
        numbers = new int[5];
        
        numbers[0] = 12;
        numbers[1] = 17;
        numbers[2] = 8;
        numbers[3] = 101;
        numbers[4] = 33;

        System.out.println(Arrays.toString(numbers));
    }
}