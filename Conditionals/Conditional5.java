//In a computer game, each gamer has an army of units.
//Write a program that will classify the army of your enemies corresponding to the following rules:
//Units: Category
//less than 1: no army
//from 1 to 19: pack
//from 20 to 249: throng
//from 250 to 999: zounds
//1000 and more: legion
//The program should read the number of units and output the corresponding category.

package BooleanStudies;

import java.util.Scanner;

public class Conditional5 {
	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in); 
	    int num = scan.nextInt();
	    
	    if (num < 1) {
	        System.out.println("no army"); 
	    } else if (1 <= num && num <= 19) {
	        System.out.println("pack"); 
	    } else if (20 <= num && num <= 249) {
	        System.out.println("throng"); 
	    } else if (250 <= num && num <= 999) {
	        System.out.println("zounds"); 
	    } else {
	        System.out.println("legion"); 
	    }
	    scan.close();
	}
}
