//Given three natural numbers A, B, C. Determine if a triangle with these sides can exist.
//If the triangle exists, output the YES string, and otherwise, output NO.
//A triangle is valid if the sum of its two sides is greater than the third 
//side. If three sides are A, B and C, then three conditions should be met.

package BooleanStudies;

import java.util.Scanner;

public class Boolean1 {
	
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Validate your triangle: ");
        System.out.println("First triangle side:");
        int a = scan.nextInt();
        System.out.println("First second side:");
        int b = scan.nextInt();
        System.out.println("First third side:");
        int c = scan.nextInt();
        
        System.out.println("All sides informed draw a triangle?");
        
        if (a + b > c) {
            if (a + c > b) {
                if (b + c > a) {
                    System.out.println("YES"); 
                } else {
                    System.out.println("NO"); 
                }       
            } else {
                System.out.println("NO"); 
            }
        } else {
            System.out.println("NO"); 
        }
        scan.close();
    }
}




