//Wow! This problem is kind of tricky. If you're ready to put your thinking cap on, brace 
//yourself and good luck! Otherwise, you can skip it for now and return any time later
//Imagine a chocolate bar. Remember how it is usually split into smaller bits by a special grid?
//Now think of chocolate as an N x M rectangle divided into little segments: N segments in 
//length and M in width. Each segment is 1x1 and unbreakable. Find out whether it is possible to 
//break off exactly K segments from the chocolate with a single straight line: vertical or horizontal.
//Input data format
//The program gets an input of three integers: N, M, K.
//Output data format
//The program must output one of the two words: YES or NO.

package BooleanStudies;

import java.util.Scanner;

public class Conditional4 {
	public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        
        int n = scan.nextInt(); 
        int m = scan.nextInt(); 
        int k = scan.nextInt(); 
        
        if (m * n >= k) {
            if (k % m == 0 || k % n == 0) {
                System.out.println("YES");   
            } else {
                System.out.println("NO");       
            }
        } else {
            System.out.println("NO");   
        } 
        scan.close();
    }
}
