//You are given coordinates of two queens on a chess board. Find out whether or not they hit each other.
//Input data format
//Four integer numbers x_1, y_1, x_2, y_2x 
//Output data format
//Type "YES" (uppercase) if they hit each other or "NO" if they don't.
//You may need a method that calculates the absolute value of the number, so here it is:

package BooleanStudies;

import java.util.Scanner;

public class Conditional2 {
	public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        
        int x1 = scan.nextInt(); 
        int y1 = scan.nextInt(); 
        int x2 = scan.nextInt(); 
        int y2 = scan.nextInt(); 
        
        if (Math.abs(x2 - x1) == Math.abs(y2 - y1)) {
            System.out.println("YES"); 
        } else if (Math.abs(x2 - x1) == 0 || Math.abs(y2 - y1) == 0) {
            System.out.println("YES"); 
        } else {
            System.out.println("NO"); 
        }
        
        scan.close();
    }
}
