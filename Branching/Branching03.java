//Write a program that reads a sequence of integer numbers in a loop and adds up all numbers. If a new number is equal to 0, the program must stop the loop and output the accumulated sum. When the sum is equal or exceeded 1000 (the barrier), the program should also stop and output the value equal to sum – 1000.

//Note, the input can contain extra numbers. Just ignore them.

//Sample Input 1:
//800
//101
//102
//300
//0
//Sample Output 1:
//3

import java.util.Scanner;

class Branching03 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        
        boolean flag = false;
        int num = 0;
        int sum = 0;
        
        while (!flag) {
            num = scan.nextInt();
            if (flag) {
                continue;
            } else if (num == 0) {
                flag = true;
                continue;
            } else if (sum >= 1000) {
                flag = true;
                continue;
            } else {
                sum += num;
            }
        }
        
        if (sum >= 1000) {
            sum -= 1000;
        }
        System.out.println(sum);
    }
}
