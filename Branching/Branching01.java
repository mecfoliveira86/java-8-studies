//Write a program that prints a part of the sequence 1 2 2 3 3 3 4 4 4 4 5 5 5 5 5 ... (the number is repeated as many times, to what it equals to). The input to the program is a positive integer n: the number of the elements of the sequence the program should print. Output the sequence of numbers, written in a single line, space-separated.

//For example, if n = 7, then the program should output 1 2 2 3 3 3 4.

import java.util.Scanner;

class Branching01 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        
        int num = scan.nextInt();
        int counter = 1;
        int cycles = 0;
        boolean flag = false; 
        
        while (counter <= num && !flag) {
            for (int j = 0; j < counter; j++) {
                System.out.print(counter + " ");
                cycles += 1;
                if (cycles == num) {
                    flag = true;
                    break;
                }
            }
            counter++;
            
        }
    }
}
