//Print the sum of all integers from a to b including both.

//It is guaranteed that a < b in all test cases.

import java.util.Scanner;

class ForLoop4 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        
        int numBegin = scan.nextInt(); 
        int numEnd = scan.nextInt(); 
        int result = 0; 
        
        for (int i = numBegin; i <= numEnd; i++) {
            result += i;    
        }
        
        System.out.println(result);
    }
}