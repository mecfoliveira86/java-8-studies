//Given the sequence of natural numbers. Find the sum of numbers divisible by 6. The input is the number of elements in the sequence, and then the elements themselves. In this sequence, there is always a number divisible by 6.

import java.util.Scanner;

class ForLoop5 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        
        int numbers = scan.nextInt();
        int num = 0;
        int result = 0;
        
        for (int i = 0; i < numbers; i++) {
            num = scan.nextInt();
            if (num % 6 == 0) {
                result += num;        
            }    
        }
        System.out.println(result);
    }
}