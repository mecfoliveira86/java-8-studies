import java.util.Scanner;

class ForLoop2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        
        int part1 = scan.nextInt(); 
        int part2 = scan.nextInt(); 
        
        for (int i = part1; i <= part2; i++) {
            if (i % 3 == 0 && i % 5 == 0) {
                System.out.println("FizzBuzz"); 
            } else if (i % 5 == 0) {
                System.out.println("Buzz"); 
            } else if (i % 3 == 0) {
                System.out.println("Fizz"); 
            } else {
                System.out.println(i); 
            }
        }
    }
}