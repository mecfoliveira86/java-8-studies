import java.util.Scanner;

class ForLoop1 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        
        int notes = scan.nextInt(); 
        int note = 0;
        int noteA = 0;
        int noteB = 0;
        int noteC = 0;
        int noteD = 0;
        
        for (int i = 0; i < notes; i++) {
            note = scan.nextInt();
            
            switch (note) {
                case 2:
                    noteD += 1;
                    break;
                
                case 3:
                    noteC += 1;
                    break;
                
                case 4:
                    noteB += 1;
                    break;
                
                case 5:
                    noteA += 1;
                    break;
                
                default:
                    break;
            }
        }
        System.out.print(noteD + " " + noteC + " " + noteB + " " + noteA);
    }
}